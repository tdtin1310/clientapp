export default {
  methods: {
    resetTranslateStyle(elementId) {
      document.getElementById(elementId).style.transform = 'translateX(' + 0 + 'px' + ') translateY(' + 0 + 'px' + ')';
    },

    buildTransformStyle(rotateDeg, scale) {
      return "rotate(" + rotateDeg + "deg)" + "scale(" + scale + ")";
    },

    arrayRemove(arr, value) {
      return arr.filter(function (ele) {
        return ele != value;
      });

    }
  }
}
