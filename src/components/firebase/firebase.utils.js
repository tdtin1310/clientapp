import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'
import 'firebase/messaging'

var firebaseConfig = {
  apiKey: "AIzaSyBiX8gVlbuLSKXDoU_j2x0m3U8NRKb4nAE",
  authDomain: "mret-8c757.firebaseapp.com",
  databaseURL: "https://mret-8c757.firebaseio.com",
  projectId: "mret-8c757",
  storageBucket: "mret-8c757.appspot.com",
  messagingSenderId: "550118322906",
  appId: "1:550118322906:web:b83e06edc06fbb24ccabe3",
  measurementId: "G-6XW93L72JS"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
export const auth = firebase.auth();
export const firestore = firebase.firestore();
export default firebase;

export const signInAnonymously = () => auth.signInAnonymously();

export const waitForScanning = async (sessionId, callback) => {
  console.log(`sessions/${sessionId}`)
  if (auth.currentUser.isAnonymous) {
    console.log("waiting for scanning");
    const userRef = await firestore.doc(`sessions/${sessionId}`)

    var unsubcribered = userRef.onSnapshot(async snapshot => {
      if (snapshot.exists) {
        console.log("Scanned");

        //delete anonymous user
        auth.currentUser.delete();

        var email = await snapshot.get("email");
        var pass = await snapshot.get("password");

        await unsubcribered();
        await auth.signInWithEmailAndPassword(email, pass);
        console.log("Logged in with credentials");
        console.log(auth.currentUser);
        //delete document that contains user credential
        userRef.delete();

        getImage(auth.currentUser).then(urls => {
          callback(urls);
        });
        listenToUploadImageEvent(callback);

      }
    }, err => {
      console.log("Permission Revoked");
      console.log(err)
    });
  }

}

export const listenToUploadImageEvent = (callback) => {
  console.log("listen to upload event")
  firestore.doc(`users/${auth.currentUser.uid}`).onSnapshot(snapshot => {
    console.log("New Image has been uploaded");
    getImage(auth.currentUser).then(urls => callback(urls));
  })
}



export const getImage = (user) => {

  var images = [];
  return firebase.storage().ref(user.uid).listAll()
    .then(e => {
      var promises = [];
      e.items.forEach(item => {
          promises.push(item.getDownloadURL().then(url => {
            return url;
          }));
        }
      );
      return Promise.all(promises);
    })

}
